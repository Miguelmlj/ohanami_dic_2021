import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
part 'estados.freezed.dart';

//ESTADOS PARA OHANAMI================================================

@freezed
class Estado with _$Estado {
  const factory Estado() = Login;
  const factory Estado.iniciandome(String nombre, String contrasena) =
      Iniciandome;
  const factory Estado.paginaprincipal() = PaginaPrincipal;
  const factory Estado.paginaregistro() = PaginaRegistro;
  const factory Estado.registrandome(String nombre, String contrasena) =
      Registrandome;
  const factory Estado.paginaerroracceso(String mensaje) = PaginaErrorAcceso;
  const factory Estado.paginarespregistro(String mensajeRegistro) =
      PaginaRespRegistro;

  const factory Estado.paginanuevapartida() = PaginaNuevaPartida;
  const factory Estado.paginanombrarjugadores(
          int cantidadJugadores, List<String> nombreJugadores) =
      PaginaNombrarJugadores;

  const factory Estado.paginarondaprimera(
      List<String> listaConJugadores, int ronda) = PaginaRondaPrimera;

  const factory Estado.errornombresjugadores(String mensajeValidacion) =
      ErrorNombresJugadores;

  const factory Estado.paginacartasjugadorr1(
      List<int> numeroDeCartas,
      String nombreJugador,
      int cantidadCartasRepartir,
      int ronda) = PaginaCartasJugadorR1;

  const factory Estado.errorjugadorseleccionado(String msj) =
      ErrorJugadorSeleccionado;

  const factory Estado.cargandopartidaterminada() = CargandoPartidaTerminada;

  const factory Estado.errorconexionpartidat(String msj, String msj2) =
      ErrorConexionPartidaT;

  const factory Estado.partidaterminada(List<dynamic> partidaTerminada) =
      PartidaTerminada;

  const factory Estado.paginamispartidas(List<dynamic> misPartidas) =
      PaginaMisPartidas;

  const factory Estado.verdetallespartida(
      List<dynamic> detallePartida, int index) = VerDetallesPartida;

  const factory Estado.estadisticajugadormostrada(
      List<dynamic> lista, int indice, int index) = EstadisticaJugadorMostrada;

  const factory Estado.eliminandopartida(int indice) = EliminandoPartida;

  const factory Estado.partidaeliminada(String mensaje) = PartidaEliminada;
}
