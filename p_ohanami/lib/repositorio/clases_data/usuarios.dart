import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:p_ohanami/repositorio/clases_data/exp_clases.dart';

class Usuarios {
  String nombre;
  String contrasena;
  List<PartidasJugador> partidas;
  Usuarios({
    required this.nombre,
    required this.contrasena,
    required this.partidas,
  });
  

  Usuarios copyWith({
    String? nombre,
    String? contrasena,
    List<PartidasJugador>? partidas,
  }) {
    return Usuarios(
      nombre: nombre ?? this.nombre,
      contrasena: contrasena ?? this.contrasena,
      partidas: partidas ?? this.partidas,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nombre': nombre,
      'contrasena': contrasena,
      'partidas': partidas.map((x) => x.toMap()).toList(),
    };
  }

  factory Usuarios.fromMap(Map<String, dynamic> map) {
    return Usuarios(
      nombre: map['nombre'] ?? '',
      contrasena: map['contrasena'] ?? '',
      partidas: List<PartidasJugador>.from(map['partidas']?.map((x) => PartidasJugador.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Usuarios.fromJson(String source) => Usuarios.fromMap(json.decode(source));

  @override
  String toString() => 'Usuarios(nombre: $nombre, contrasena: $contrasena, partidas: $partidas)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Usuarios &&
      other.nombre == nombre &&
      other.contrasena == contrasena &&
      listEquals(other.partidas, partidas);
  }

  @override
  int get hashCode => nombre.hashCode ^ contrasena.hashCode ^ partidas.hashCode;
}
