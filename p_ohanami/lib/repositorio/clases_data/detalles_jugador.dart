import 'dart:convert';

import 'package:p_ohanami/repositorio/clases_data/exp_clases.dart';

class DetallesJugador {
  String nombreJugador;
  int puntuacionFinal;
  Cartas ronda1;
  Cartas ronda2;
  Cartas ronda3;
  DetallesJugador({
    required this.nombreJugador,
    required this.puntuacionFinal,
    required this.ronda1,
    required this.ronda2,
    required this.ronda3,
  });
  

  DetallesJugador copyWith({
    String? nombreJugador,
    int? puntuacionFinal,
    Cartas? ronda1,
    Cartas? ronda2,
    Cartas? ronda3,
  }) {
    return DetallesJugador(
      nombreJugador: nombreJugador ?? this.nombreJugador,
      puntuacionFinal: puntuacionFinal ?? this.puntuacionFinal,
      ronda1: ronda1 ?? this.ronda1,
      ronda2: ronda2 ?? this.ronda2,
      ronda3: ronda3 ?? this.ronda3,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nombreJugador': nombreJugador,
      'puntuacionFinal': puntuacionFinal,
      'ronda1': ronda1.toMap(),
      'ronda2': ronda2.toMap(),
      'ronda3': ronda3.toMap(),
    };
  }

  factory DetallesJugador.fromMap(Map<String, dynamic> map) {
    return DetallesJugador(
      nombreJugador: map['nombreJugador'] ?? '',
      puntuacionFinal: map['puntuacionFinal']?.toInt() ?? 0,
      ronda1: Cartas.fromMap(map['ronda1']),
      ronda2: Cartas.fromMap(map['ronda2']),
      ronda3: Cartas.fromMap(map['ronda3']),
    );
  }

  String toJson() => json.encode(toMap());

  factory DetallesJugador.fromJson(String source) => DetallesJugador.fromMap(json.decode(source));

  @override
  String toString() {
    return 'DetallesJugador(nombreJugador: $nombreJugador, puntuacionFinal: $puntuacionFinal, ronda1: $ronda1, ronda2: $ronda2, ronda3: $ronda3)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is DetallesJugador &&
      other.nombreJugador == nombreJugador &&
      other.puntuacionFinal == puntuacionFinal &&
      other.ronda1 == ronda1 &&
      other.ronda2 == ronda2 &&
      other.ronda3 == ronda3;
  }

  @override
  int get hashCode {
    return nombreJugador.hashCode ^
      puntuacionFinal.hashCode ^
      ronda1.hashCode ^
      ronda2.hashCode ^
      ronda3.hashCode;
  }
}
