import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaRonda1 extends StatelessWidget {
  const PantallaRonda1(this.listaJugadores, this.ronda,{Key? key}) : super(key: key);
  final int ronda;
  final List<String> listaJugadores;

  @override
  Widget build(BuildContext context) {
    

    var estiloColorAppBar = Colors.black54;
    if (ronda == 1) {
      estiloColorAppBar = Colors.black54;
    }
    if (ronda == 2) {
      estiloColorAppBar = Colors.red.shade300;
    }
    if (ronda == 3) {
      estiloColorAppBar = Colors.blueGrey;
    }

    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text('Ronda $ronda')),
          backgroundColor: estiloColorAppBar,
        ),
        body: JugadoresContenedor(listaJugadores),
        bottomNavigationBar: OpcionesAlert());
  }
}

class JugadoresContenedor extends StatefulWidget {
  const JugadoresContenedor(this.listaJugadores,{Key? key}) : super(key: key);
  final List<String> listaJugadores;

  @override
  State<JugadoresContenedor> createState() => _JugadoresContenedorState(listaJugadores);
}

class _JugadoresContenedorState extends State<JugadoresContenedor> {
  List<String> listaJugadores;
  _JugadoresContenedorState(this.listaJugadores);

  @override
  Widget build(BuildContext context) {
    

    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("Pulsa en el jugador para establecer número de cartas", style: estiloMensaje),
        ),
        Icon(
          Icons.insert_chart_outlined_rounded,
          size: 100.0,
          color: Colors.black54,
        ),
        Expanded(
            child: ListView.builder(
          itemCount: listaJugadores.length,
          itemBuilder: (BuildContext context, int index) {
            String key = listaJugadores.elementAt(index);
            return new Column(
              children: <Widget>[
                //SizedBox(height: 10.0,),
                ListTile(
                  title: Text(listaJugadores[index] + '!', style: estiloMensaje),
                  subtitle: Text('Selecciona para numerar cartas del jugador'),
                  leading: Icon(Icons.personal_injury_rounded, color: Colors.red.shade300,),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () {
                    print(index);
                    context
                        .read<BlocUno>()
                        .add(JugadorSeleccionadoR1(jugadorSeleccionado: index));
                  },
                ),
                Divider(),
              ],
            );
          },
        )),
      ],
    );
  }
}

class OpcionesAlert extends StatelessWidget {
  const OpcionesAlert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
               style: ElevatedButton.styleFrom(
                    primary: Colors.red.shade300,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
              onPressed: () => _mostrarAdvertencia(context),
              onLongPress: () {
                context.read<BlocUno>().add(SalirDePartida());
              },
              child: Text('Ir a inicio'),
            ),
          ),
          SizedBox(
            width: 190.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
               style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () {
                  _mostrarAdvertencia2(context);
                },
                onLongPress: () {
                  context.read<BlocUno>().add(SiguienteRonda());
                },
                child: Text('Siguiente')),
          )
        ],
      ),
    );
  }

  void _mostrarAdvertencia(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text('Advertencia'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(salirDePartida),
                iconoAdvertencia,
              ],
            ),
            actions: [
              ElevatedButton(
                 style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Aceptar'),
              ),
            ],
          );
        });
  }

  void _mostrarAdvertencia2(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text('Mensaje'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(child: Text(siguienteRonda)),
                iconoAdvertencia,
              ],
            ),
            actions: [
              ElevatedButton(
                 style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Aceptar'),
              ),
            ],
          );
        });
  }
}
