import 'package:flutter/material.dart';

import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaEliminandoPartida extends StatelessWidget {
  const PantallaEliminandoPartida(this.partidaSeleccionada,{ Key? key }) : super(key: key);
  final int partidaSeleccionada;
  

  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
          title: Center(child: Text('Pantalla de Espera')),
          backgroundColor: Colors.black54),
      body: Eliminando(partidaSeleccionada),
    );
  }
}

class Eliminando extends StatelessWidget {
  const Eliminando(this.partidaSeleccionada,{ Key? key }) : super(key: key);
  final int partidaSeleccionada;
  
  @override
  Widget build(BuildContext context) {
    context.read<BlocUno>().add(IntentarEliminarPartida(partidaSeleccionada:partidaSeleccionada));

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
                width: 200, height: 200, child: CircularProgressIndicator(
                  color: Colors.red.shade300,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Text(
                msj1ElimPartida,
                 style: estiloMensaje
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: ListTile(
              title: Center(child: Text(msj2ElimPartida, style: estiloMensaje)),
              subtitle: Center(
                  child: Text(msj3ElimPartida)),
            ),
          )
        ],
      ),
    );
  }
}