import 'package:flutter/material.dart';
import 'package:p_ohanami/app/constantes.dart';
import 'package:p_ohanami/app/estilos.dart';
import 'package:p_ohanami/bloc/bloc.dart';
import 'package:p_ohanami/bloc/estados.dart';
import 'package:p_ohanami/bloc/eventos.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PantallaListaMisPartidas extends StatelessWidget {
  const PantallaListaMisPartidas(this.partidas, {Key? key}) : super(key: key);
  final List<dynamic> partidas;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Mis Partidas')),
        backgroundColor: Colors.red.shade300,
        actions: [MenuPrin()],
      ),
      body: CuerpoListaPartidas(partidas),
    );
  }
}

class MenuPrin extends StatelessWidget {
  const MenuPrin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: ChoiceChip(
        label: Text('Regresar'),
        selected: false,
        onSelected: (select) {
          //context.read<BlocUno>().add(CerrandoSesion());
          context.read<BlocUno>().add(SalirDePartida());
        },
      ),
    ));
  }
}

class CuerpoListaPartidas extends StatelessWidget {
  const CuerpoListaPartidas(this.partidas, {Key? key}) : super(key: key);
  final List<dynamic> partidas;

  @override
  Widget build(BuildContext context) {
    if (partidas.isEmpty)
      return Container(
        child: Column(
          children: [
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Text(
                'No hay partidas',
                style: estiloMensaje,
              )),
            ),
            SizedBox(
              height: 50.0,
            ),
            iconoMensaje,
            SizedBox(height: 35),
            Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(fontSize: 20)),
                onPressed: () {
                  context.read<BlocUno>().add(SalirDePartida());
                },
                child: const Text('Regresar'),
              ),
            ),
          ],
        ),
      );

    return ListView.builder(
        itemCount: partidas.length,
        itemBuilder: (context, index) {
          return Card(
            elevation: 10.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Column(
              children: [
                ListTile(
                  trailing: Padding(
                    padding: const EdgeInsets.only(left: 50.0),
                    child: FlatButton(
                          onLongPress: (){
                           context.read<BlocUno>().add(
                              EliminarPartida(partidaSeleccionada: index));   
                          },
                          onPressed: () => _mostrarAdvertencia(context),
                          child: Icon(Icons.highlight_remove_sharp, color: Colors.grey),
                      ),
                  ),
                  leading: iconoGame,
                  title: Text(
                    'Fecha: ${partidas[index]['fecha']}',
                    style: estiloMensaje,
                  ),
                  subtitle: Text(
                      '# Jugadores: ${partidas[index]['jugadores'].length}'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    
                    FlatButton(
                      child: Text('Ver Detalles'),
                      onPressed: () {
                        context.read<BlocUno>().add(
                            VerDetallesDePartida(partidaSeleccionada: index));
                      },
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  _mostrarAdvertencia(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text('Advertencia'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(eliminarPartida),
                iconoAdvertencia,
              ],
            ),
            actions: [
              ElevatedButton(
                 style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Aceptar'),
              ),
            ],
          );
        });
  }
}
